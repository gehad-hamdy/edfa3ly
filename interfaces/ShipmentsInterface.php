<?php

namespace edfa3ly\interfaces;


interface ShipmentsInterface
{
    public function createShipmentInfo();

    public function getShipmentTrackingDetails($courierID);

    public function getShipmentWayBill($courierID);
}