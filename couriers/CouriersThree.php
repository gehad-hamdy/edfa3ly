<?php

namespace edfa3ly\couriers {

    use edfa3ly\Shipments;

    class CouriersThree extends Shipments
    {
        /**
         * @param $courierID
         */
        public function createShipmentAndGetWayBill($courierID)
        {
            parent::createShipmentInfo(); // TODO: Change the autogenerated stub

            parent::getShipmentWayBill($courierID); // TODO: Change the autogenerated stub
        }

        /**
         * @param $register_number
         */
        public function RegisterNumberTrackingDetails($register_number)
        {
            // ToDo: track the shipment using it's register number
        }

        /**
         * @param $courierID
         */
        public function getShipmentTrackingDetails($courierID)
        {
            parent::getShipmentTrackingDetails($courierID); // TODO: Change the autogenerated stub
        }

    }
}