<?php

namespace edfa3ly {

    use edfa3ly\interfaces\ShipmentsInterface;

    class Shipments implements ShipmentsInterface
    {
        public function createShipmentInfo()
        {
            // TODO: Implement createShipmentAndGetWayBill() method.
        }

        /**
         * @param $courierID
         */
        public function getShipmentTrackingDetails($courierID)
        {
            // TODO: Implement getShipmentTrackingDetails() method.
        }

        /**
         * @param $courierID
         */
        public function getShipmentWayBill($courierID)
        {
            // TODO: Implement getShipmentWayBill() method.
        }

        protected function __construct()
        {
        }

        private function __clone()
        {
        }

        private function __wakeup()
        {
        }
    }
}