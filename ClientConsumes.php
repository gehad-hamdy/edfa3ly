<?php

namespace edfa3ly;

use edfa3ly\interfaces\ShipmentsInterface;

class ClientConsumes
{
    /**
     * @param ShipmentsInterface $shipment
     */
    public function createShipment($shipment)
    {
        return $shipment->createShipmentInfo();
    }

    /**
     * @param ShipmentsInterface $shipment
     * @param int $couriersID
     */
    public function trackShipment($shipment, $couriersID)
    {
        if (!empty($couriersID)) {
            return $shipment->getShipmentTrackingDetails($couriersID);
        }
    }
}